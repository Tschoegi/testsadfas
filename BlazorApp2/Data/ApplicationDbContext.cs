﻿using BlazorApp2.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BlazorApp2.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Model> Models { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Engine> Engines { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Brand>().HasData(
                new Brand {Id =1, Name = "BMW" },
                new Brand {Id =2, Name = "VW" }
                );
            builder.Entity<Engine>().HasData(
                new Engine { Id =1, Name = "V8" ,Price = 500}
                );
        }
    }
    
}