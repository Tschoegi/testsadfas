﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp2.Data.Models
{
    public class Brand
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string? Name { get; set; }
        public ICollection<Model> Models { get; set; }
    }
}
