﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp2.Data.Models
{
    public class Engine
    {
        [Key]
        public int Id { get; set; }
        [Required,StringLength(50)]
        public string? Name { get; set; }
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }  
        public ICollection<Model> Models { get; set; }   
    }
}
