﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp2.Data.Models
{
    public class Model
    {
        [Key]
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string? Name { get; set; }
        [Required,DataType(DataType.Currency)]
        public decimal? Price { get; set; }
        public Engine? Engine { get; set; }
        public Brand? Brand { get; set; }
    }
}
